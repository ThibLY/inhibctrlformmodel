# InhibCtrlFormModel

This repository contains :

- The appendix of the paper titled "A Formal Probabilistic Model of the Inhibitory Control Circuit in the Brain" presented at BIOINFORMATICS 2023;
- 8 Prism model files.

These files have the ".pm" extension and can be edited with any editing tools.
These models have been model checked using Prism framework. The storm model checker has also been used and shows similar result.

Several parameters of Prism were changed for the model checking to perform. The template Prism command line used for this work is the following:  
"  
prism model.pm -pf 'PCTL formula' -ex -javamaxmem 12g -cuddmaxmem 12g -epsilon 1e-12 -javastack 1g -maxiters 30000  
"  

The storm equivalent command line is:  
"  
storm --prism model.pm --prop "PCTL formula"  
"  

Thank you for reading!
