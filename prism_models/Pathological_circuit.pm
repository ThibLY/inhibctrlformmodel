dtmc

module Time
time:[0..10];
[to] time<10 -> 1:(time'=time+1);
[to] time=10 -> 1:(time'=1);
endmodule

module entry
//Input spike that follow a Poissonian law with 7 as parameter
//Prism does not support more than 15 digit after the dot for this command
t1:[0..3000];
[to] true -> 0.000072991952613:(t1'=1) + 0.003437086558390:(t1'=2) + 0.021604031452484:(t1'=3) + 0.059540362609726:(t1'=4) + 0.104444862957054:(t1'=5) + 0.137676978041126:(t1'=6) + 0.149002779674338:(t1'=7) + 0.139586531950597:(t1'=8) + 0.117116124452909:(t1'=9) + 0.26751825035076304:(t1'=10);
endmodule

const int wSNpcSTr=30;
const int tau=80;
const double r=0.5;
const int wCxSTr=80;
const int wSTrGPe=40;
const int wGPeSTr=80;
const int wGPeGPe=20;
const int wSTNGPe=100;
const int wGPeSTN=40;
const int wSTrsnr=40;
const int wGPesnr=20;
const int wSTNDelay=80;
const int wDelaysnr=100;
const int wsnrTh=80;
const int wCxTh=80;
const int wstop=80;

//Global membrane potential formula for STr
formula STr_healthy=floor((-wSNpcSTr*t1)+(wCxSTr*t1)+(-wGPeSTr*n_GPe)+(r*potential_STr*(1-(floor(potential_STr/tau)/10))));
formula STr_patho=floor((wCxSTr*t1)+(-wGPeSTr*n_GPe)+(r*potential_STr*(1-(floor(potential_STr/tau)/10))));

//Global membrane potential formula for GPe
formula GPe_f=floor((-wSTrGPe*n_STr)+(wSTNGPe*n_STN)+(wGPeGPe*n_GPe)+(r*potential_GPe*(1-(floor(potential_GPe/tau)/10))));

//Global membrane potential formula for STN
formula STN_f=floor((-wGPeSTN*n_GPe)+(r*potential_STN*(1-(floor(potential_STN/tau)/10))));

//Global membrane potential formula for SNpr
formula SNpr_f=floor(-((wSTrsnr*n_STr)+(wGPesnr*n_GPe))+(wDelaysnr*n_Delay)+(r*potential_SNpr*(1-(floor(potential_SNpr/tau)/10))));

//Global membrane potential formula for Delay
formula Delay_f=floor(wSTNDelay*n_STN);

//Global membrane potential formula for Th
formula Th_f=floor((-wsnrTh*n_SNpr)+(wCxTh*t1)+(r*potential_Th*(1-(floor(potential_Th/tau)/10))));

module STr
potential_STr:[0..800];
n_STr:[0..10];
[to] n_STr>=0 -> 0.3:(n_STr'=STr_healthy<0?0:STr_healthy>800?10:floor(STr_healthy/tau)) & (potential_STr'=STr_healthy<0?0:STr_healthy>800?800:STr_healthy) + 0.7:(n_STr'=STr_patho<0?0:STr_patho>800?10:floor(STr_patho/tau)) & (potential_STr'=STr_patho<0?0:STr_patho>800?800:STr_patho);
endmodule

module GPe
potential_GPe:[0..800];
n_GPe:[0..10];
[to] n_GPe>=0 -> 1:(n_GPe'=GPe_f<0?0:GPe_f>800?10:floor(GPe_f/tau)) & (potential_GPe'=GPe_f<0?0:GPe_f>800?800:GPe_f);
endmodule

module STN
potential_STN:[0..800];
n_STN:[0..10];
[to] time !=9 & n_STN>=0 -> 1:(n_STN'=STN_f<0?0:STN_f>800?10:floor(STN_f/tau)) & (potential_STN'=STN_f<0?0:STN_f>800?800:STN_f);
//Here is another set of guard/update to add the input of the stop signal
[to] time=9 -> 1:(potential_STN'=800) & (n_STN'=10);
endmodule

module SNpr
potential_SNpr:[0..800];
n_SNpr:[0..10];
[to] n_SNpr>=0 -> 1:(n_SNpr'=SNpr_f<0?0:SNpr_f>800?10:floor(SNpr_f/tau)) & (potential_SNpr'=SNpr_f<0?0:SNpr_f>800?800:SNpr_f);
endmodule

module Delay
potential_Delay:[0..800];
n_Delay:[0..10];
[to] n_Delay>=0 -> 1:(n_Delay'=Delay_f<0?0:Delay_f>800?10:floor(Delay_f/tau)) & (potential_Delay'=Delay_f<0?0:Delay_f>800?800:Delay_f);
endmodule

module Th
potential_Th:[0..800];
n_Th:[0..10];
[to] n_Th>=0 -> 1:(n_Th'=Th_f<0?0:Th_f>800?10:floor(Th_f/tau)) & (potential_Th'=Th_f<0?0:Th_f>800?800:Th_f);
endmodule

rewards "Inhibited"
	n_Th < 4 & time > 0 :1;
endrewards
